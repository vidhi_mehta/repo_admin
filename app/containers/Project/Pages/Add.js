import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { Alert, AlertTitle } from '@material-ui/lab';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import * as actionTypes from '../actions';

const useStyles = makeStyles({
  root: {
    marginTop: 16,
    marginBottom: 16,
    padding: 16,
    boxShadow:
      '0px 3px 5px -1px rgba(0,0,0,0.2), 0px 6px 10px 0px rgba(0,0,0,0.14), 0px 1px 18px 0px rgba(0,0,0,0.12)',
  },
  button: {
    marginTop: 16,
    background: '#FF8686',
  },
});

const Add = ({
  title,
  setTitle,
  addItem,
  editItem,
  edit,
  error,
  setError,
  apiData,
}) => {
  const { useState } = React;
  const [showMessage, setShowMessage] = useState(false);
  const classes = useStyles();
  const handleChange = event => {
    const title = event.target.value;
    setTitle(title);
    if (title.length === 0) {
      setError('Please enter event');
    } else {
      setError('');
    }
  };

  const handleClick = () => {
    console.log('in handleClick');

    if (title.length === 0) {
      setError('Please enter event');
      return;
    }
    addItem(title);
    setShowMessage(true);
    // if (edit) {
    //     console.log("in edit")
    //     editItem(title);
    // } else {
    //     console.log("else in edit")

    //     addItem(title);
    //     setShowMessage(true)
    // }
  };
  const getAPIDataClick = () => {
    apiData();
    setShowMessage(true);
  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setShowMessage(false);
  };
  return (
    <Container maxWidth="sm" className={classes.root}>
      <Grid container alignItems="center">
        <Grid item md={12}>
          <TextField
            value={title}
            onChange={handleChange}
            error={!!error}
            helperText={error}
            id="outlined-basic"
            fullWidth
            label="Enter Event"
            multiline
            variant="outlined"
          />
        </Grid>
        <Grid item md={12}>
          <Button
            className={classes.button}
            variant="contained"
            color="primary"
            onClick={handleClick}
          >
            {/* {edit ? "Edit Event" : "Add Event"} */}Add Event
          </Button>
        </Grid>
        <Grid item md={12}>
          <Button
            className={classes.button}
            variant="contained"
            color="primary"
            onClick={getAPIDataClick}
          >
            View API data
          </Button>
        </Grid>
        <Snackbar
          open={showMessage}
          autoHideDuration={2000}
          onClose={handleClose}
        >
          <Alert severity="success">
            <AlertTitle>Success</AlertTitle>
            Event added in Table — <strong>check it out!</strong>
          </Alert>
        </Snackbar>
      </Grid>
    </Container>
  );
};

const mapStateToProps = state => {
  console.log('in add map state to props', state);
  return {
    title: state.items.title,
    edit: state.items.edit,
    error: state.items.error,
  };
};

const mapDispatchToProps = dispatch => ({
  setTitle: title => dispatch(actionTypes.setTitle(title)),
  setError: error => dispatch(actionTypes.setError(error)),
  addItem: title => dispatch(actionTypes.addItem(title)),
  editItem: title => dispatch(actionTypes.editItem(title)),
  apiData: () => dispatch(actionTypes.apiData()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Add);
