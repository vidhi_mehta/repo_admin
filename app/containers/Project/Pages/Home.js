import React from 'react';
import Container from '@material-ui/core/Container';
import { connect } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import MaterialTable from 'material-table';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Alert, AlertTitle } from '@material-ui/lab';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import * as actionTypes from '../actions';

const drawerWidth = 240;

const useStyles = makeStyles({
  container: {
    padding: 50,
  },
  list: {
    width: 250,
    height: 500,
  },
  fullList: {
    width: 'auto',
  },
  item: {
    paddingTop: 20,
    paddingBottom: 20,
  },
  drawerPaper: {
    width: drawerWidth,
    height: '90%',
  },
});

function Home({
  todoList,
  setTitle,
  setItem,
  setEdit,
  deleteItem,
  editItem,
  title,
  addItem,
  edit,
  error,
  setError,
}) {
  const { useState } = React;
  const [selectedRow, setSelectedRow] = useState(null);
  const [showMessage, setShowMessage] = useState(false);
  const [editText, setEditText] = useState(false);

  const classes = useStyles();
  const handleChange = event => {
    console.log("handleChange")
    const title = event.target.value;
    setTitle(title);
    if (title.length === 0) {
      setError('Please enter event');
    } else {
      setError('');
    }
  };
  const handleClick = () => {
    console.log("handleClick")
    if (title.length === 0) {
      setError('Please enter event');
      return;
    }
    if (edit) {
      editItem(title);
    } else {
      addItem(title);
      setShowMessage(true);
    }
  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setShowMessage(false);
  };
  const handleEdit = item => {
    setEditText(true);
    console.log('handle edit', item);
    setTitle(item.value);
    setEdit();
    setItem(item);
  };

  const handleDelete = item => {
    setItem(item);
    deleteItem(item);
  };
  function searchInData(e) { }

  const dataColumns = [
    {
      width: 50,
      cellStyle: {
        padding: 0,
      },
      headerStyle: {
        padding: 0,
      },
      sorting: false,
      filtering: false,
    },
    {
      title: 'ID',
      field: 'id',
      render: rowData => (
        <div>
          {['bottom'].map(anchor => (
            <React.Fragment key={anchor}>
              <Button onClick={toggleDrawer(anchor, true, rowData)}>
                {rowData.id}
              </Button>
              <Drawer
                anchor={anchor}
                open={state[anchor]}
                onClose={toggleDrawer(anchor, true, 'close')}
              >
                {console.log('list(anchor)', list(anchor))}
                {list(anchor)}
              </Drawer>
            </React.Fragment>
          ))}
        </div>
      ),
    },
    {
      title: 'Event Name',
      field: 'value',
      customSort: (a, b) => a.value.length - b.value.length,
    },
  ];
  const [state, setState] = React.useState({
    bottom: false,
  });

  const toggleDrawer = (anchor, open, data) => event => {
    console.log('in toggle drawer', anchor, data);
    if (data != '') {
      handleEdit(data);
    }

    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = anchor => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'bottom',
      })}
      role="presentation"
    // onClick={toggleDrawer(anchor, false)}
    // onKeyDown={toggleDrawer(anchor, false)}
    >
      <Container maxWidth="sm" className={classes.root}>
        <Button
          style={{ margin: 20 }}
          variant="contained"
          onClick={toggleDrawer(anchor, false, 'close')}
        >
          Close Drawer
        </Button>

        {editText && (
          <Grid container alignItems="center">
            <Grid style={{ margin: 20 }} item md={12}>
              <TextField
                value={title}
                onChange={handleChange}
                error={!!error}
                helperText={error}
                id="outlined-basic"
                fullWidth
                label="Enter Event"
                multiline
                variant="outlined"
              />
            </Grid>
            <Grid style={{ margin: 20 }} item md={12}>
              <Button
                className={classes.button}
                variant="contained"
                color="primary"
                onClick={handleClick}
              >
                {edit ? 'Edit Event' : setEditText(false)}
              </Button>
            </Grid>
          </Grid>
        )}
        {/* 
                <Typography variant="h6" color="error">Edit Text : {title}</Typography>
 */}

        <Snackbar
          open={showMessage}
          autoHideDuration={2000}
          onClose={handleClose}
        >
          <Alert severity="success">
            <AlertTitle>Success</AlertTitle>
            Event added in Table — <strong>check it out!</strong>
          </Alert>
        </Snackbar>
      </Container>
    </div>
  );
  return (
    <Container className={classes.container} maxWidth="md">
      <div style={{ maxWidth: '100%' }}>
        {console.log('hi', todoList.items)}
        <div>
          {!todoList.items.length ? (
            <Typography variant="h6" color="error">
              No Data to display
            </Typography>
          ) : (
              <MaterialTable
                onSearchChange={e => searchInData(e)}
                columns={dataColumns}
                data={todoList.items}
                options={{
                  sorting: true,
                  search: true,
                  rowStyle: rowData => ({
                    backgroundColor:
                      selectedRow === rowData.tableData.id ? '#EEE' : '#FFF',
                  }),
                }}
                onRowClick={(evt, selectedRow) =>
                  setSelectedRow(selectedRow.tableData.id)
                }
                title="Demo Title"
                actions={[
                  {
                    icon: 'more_vert',
                    tooltip: 'Edit Event',
                    onClick: (event, rowData) => {
                      console.log('in drawer', rowData.id);
                      toggleDrawer('bottom', true, 'close');
                    },
                  },
                  {
                    icon: 'delete',
                    tooltip: 'Delete Event',
                    onClick: (event, rowData) => {
                      console.log('row', rowData);
                      console.log(`You want to delete ${rowData.value}`);
                      handleDelete(rowData);
                    },
                  },
                ]}
              />
            )}
        </div>
      </div>
    </Container>
  );
}

const mapStateToProps = state => {
  console.log('in Home map state to props', state);
  return {
    todoList: state.items,
    title: state.items.title,
    edit: state.items.edit,
    error: state.items.error,
  };
};

const mapDispatchToProps = dispatch => ({
  setTitle: title => dispatch(actionTypes.setTitle(title)),
  setItem: item => dispatch(actionTypes.setItem(item)),
  deleteItem: item => dispatch(actionTypes.deleteItem(item)),
  setEdit: () => dispatch(actionTypes.setEdit()),
  setTitle: title => dispatch(actionTypes.setTitle(title)),
  setError: error => dispatch(actionTypes.setError(error)),
  addItem: title => dispatch(actionTypes.addItem(title)),
  editItem: title => dispatch(actionTypes.editItem(title)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
