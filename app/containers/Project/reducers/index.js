import * as actionTypes from '../actions/actionTypes';

const initialState = {
  items: [],
  title: '',
  item: '',
  edit: false,
  error: '',
};

const items = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_ITEM_A':
      console.log('Action', action, 'action.payload', action.payload);
      const newitem = {
        id: Date.now(),
        value: action.payload,
      };
      console.log('in add item', newitem);
      return {
        ...state,
        items: state.items.concat(newitem),
        title: '',
        error: '',
      };

    case 'API_DATA_A':
      console.log('Action', action, 'action.payload', action.payload);
      const values = action.payload;
      console.log('what is payload values comes in', values);
      const apiVal = values.map(e => ({
        id: e.id,
        value: e.title,
      }));
      console.log('in api data after map', apiVal);
      return {
        ...state,
        items: state.items.concat(apiVal),
        title: '',
        error: '',
      };

    case 'EDIT_ITEM_A':
      console.log('Action', action, 'action.payload', action.payload);

      var newList = [...state.items];
      console.log('i am new list', newList);

      var index = newList.indexOf(state.item);
      console.log('i am new index', index);

      if (index !== -1) {
        console.log('in if- i am new list', newList);
        newList[index].value = action.payload;
        return {
          ...state,
          title: '',
          edit: false,
          items: newList,
          error: '',
        };
      }
      return {
        ...state,
      };

    case 'DELETE_ITEM_A':
      console.log('Action', action, 'action.payload', action.payload);
      console.log('check value from state', state.item);
      newList = [...state.items];
      index = newList.indexOf(action.payload);
      if (index !== -1) {
        newList.splice(index, 1);
        return {
          ...state,
          items: newList,
        };
      }
      return {
        ...state,
      };

    case actionTypes.SET_TITLE:
      return {
        ...state,
        title: action.title,
      };
    case actionTypes.SET_ITEM:
      console.log('in set item', action.item);
      return {
        ...state,
        item: action.item,
        error: '',
      };
    case actionTypes.SET_ERROR:
      return {
        ...state,
        error: action.error,
      };
    case actionTypes.SET_EDIT:
      console.log('in set edit reducer');
      return {
        ...state,
        edit: true,
        error: '',
      };
    default:
      return state;
  }
};

export default items;
