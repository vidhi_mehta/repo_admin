import * as actionTypes from './actionTypes';

export const addItem = title => ({
  type: actionTypes.ADD_ITEM,
  title,
});
export const deleteItem = item => {
  console.log('i am delete item', item);
  return {
    type: actionTypes.DELETE_ITEM,
    item,
  };
};
export const editItem = item => {
  console.log('i am in edit item', item);
  return {
    type: actionTypes.EDIT_ITEM,
    item,
  };
};
export const setTitle = title => ({
  type: actionTypes.SET_TITLE,
  title,
});
export const setError = error => ({
  type: actionTypes.SET_ERROR,
  error,
});
export const setItem = item => ({
  type: actionTypes.SET_ITEM,
  item,
});
export const setEdit = () => ({
  type: actionTypes.SET_EDIT,
});
export const apiData = () => {
  console.log('in action type api data');
  return {
    type: actionTypes.API_DATA,
  };
};
